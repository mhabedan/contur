#!/usr/bin/env python
import numpy as np
import os
import matplotlib
import matplotlib.pyplot as plt
from datetime import date
from matplotlib.colors import LogNorm
import optparse


parser = optparse.OptionParser(usage="%prog [options]")
parser.add_option("-i", "--inputDir", help="Path to scan directory",
                  dest="_inDir", default="path/to/myscan00/13TeV/")
parser.add_option("-o", "--outputDir", help="Path where to dump your plots",
                  dest="_outDir", default="CONTUR_xs_scans/")
parser.add_option("-t", "--tolerance",
                  help="Minimum cross-section in fb for a process to be drawn", dest="_tolerance", default=1.)
parser.add_option("--xc", "--ignoreCache", help="Whether or not to use cached results if they exist. Can speed up processing if your logs didn't change!",
                  dest="_ignoreCache", default=False, action="store_true")
parser.add_option("--cc", "--clearCache", help="Resret the cache",
                  dest="_clearCache", default=False, action="store_true")
parser.add_option("--br", "--foldBRs", help="Whether or not to fold in the branching ratios",
                  dest="_foldBRs", default=False, action="store_true")
parser.add_option("--bsm_br", "--foldBSMBRs", help="Whether or not to fold in the BSM branching ratios ",
                  dest="_foldBSMBRs", default=False, action="store_true")
parser.add_option("--sl", "--splitLeptons", help="Leptons e, mu tau are set to l by default. Apply this flag to split them again",
                  dest="_splitLeptons", default=False, action="store_true")
parser.add_option("--mb", "--mergeBosons", help="Set W, Z, H to V",
                  dest="_mergeBosons", default=False, action="store_true")
parser.add_option("--sp", "--splitPartons", help="We normally don't care about the incoming partons, just set them to pp. Apply this flag to split them again",
                  dest="_splitPartons", default=False, action="store_true")
parser.add_option("--sa", "--splitAntiparticles", help="Particles and antiparticles are merged by default. Add this options to split them out",
                  dest="_splitAntiParticles", default=False, action="store_true")
parser.add_option("--sb", "--split_b", help="u, d, s, c, b are grouped into q by default. Add this options to split out the b",
                  dest="_splitBQuarks", default=False, action="store_true")
parser.add_option("--sq", "--splitLightQuarks", help="u, d, s, c, b are grouped into q by default. Add this options to split them out",
                  dest="_splitLightQuarks", default=False, action="store_true")
parser.add_option("--p", "--pools", help="Split into pools based on final state ? Only works with --br option",
                  dest="_splitIntoPools", default=False, action="store_true")
parser.add_option("--xy", help="Variables to scan", dest="_xy", default=None )
(opts, args) = parser.parse_args()

justLog = 1

opts._tolerance = float(opts._tolerance)
x = []
y = []
results = {}

if opts._xy is None:
  print "[ERROR] please choose which params to scan over from this list, in format 'var1,var2'"
  os.system("head -n1 %s/sampled_points.dat"%opts._inDir)
  exit(1)

postFix = ""
command = ""
if opts._foldBRs:
    postFix += "_BRs"
    command += " --br"
if opts._foldBSMBRs:
    postFix += "_BSMBRs"
    command += " --bsm_br"
if opts._splitLeptons:
    postFix += "_sL"
    command += " --sl"
if opts._mergeBosons:
    postFix += "_V"
    command += " --mb"
if opts._splitPartons:
    postFix += "_sP"
    command += " --sp"
if opts._splitAntiParticles:
    postFix += "_spA"
    command += " --sa"
if opts._splitBQuarks:
    postFix += "_spB"
    command += " --sb"
if opts._splitLightQuarks:
    postFix += "_spLq"
    command += " --sq"
if opts._xy is not None : 
  command += " --xy %s"% opts._xy


postFixPlotsDir = postFix
if opts._splitIntoPools:
    postFixPlotsDir += "_pools"
stem = "process_plots_%s_%s" % (
    str(date.today().strftime('%d%m%y')), postFixPlotsDir)

outdir = opts._outDir+"/"+stem
os.system("mkdir -p %s" % outdir)
#os.system("rm %s/*"%outdir)
# if os.path.isfile("~/index.php"):
#os.system("cp ~/index.php %s/."%outdir)
counter = -1
max_xs = 0
min_xs = 99999
maxProcs = []
for f in os.listdir(opts._inDir):
    counter += 1
    f = opts._inDir+"/"+f
    flog="."#opts._inDir+"/"+f
    # if counter> 25: break
    print(f)
    if "ANA" in f:
        continue
    if os.path.isdir(f):
        if (os.path.isfile("%s/out%s.log" % (flog, postFix))) and opts._clearCache:
            os.system("rm %s/out%s.log" % (f, postFix))
        if not (os.path.isfile("%s/out%s.log" % (f, postFix)) and (not opts._ignoreCache)):
            os.system(" contur-extract-herwig-xs-br -i %s %s > %s/out%s.log " %
                      (f, command, f, postFix))
        log = open("%s/out%s.log" % (flog, postFix))
        for logline in log.readlines():
            logline = logline.strip()
            if not "::" in logline:
                if ":" in logline:
                    continue
                if "totalXS" in logline:
                    continue
                if "Skip" in logline:
                    continue
                xs = float(logline.split(", ")[0].replace("fb", ""))
                proc = logline.split(", ")[-1]
                if xs > max_xs:
                    max_xs = xs
                    print(" --> max mx is ", max_xs, " for ", proc)
                if xs < min_xs and xs != 0:
                    min_xs = xs
                    print(" --> min mx is ", min_xs, " for ", proc)
                    if not proc in maxProcs:
                        maxProcs += [proc]
                if not proc in results.keys():
                    results[proc] = [0 for i in range(len(x)-1)]
                results[proc].append(xs)
            else:
                values = logline.split("::")[-1].split(",")
                xVal = float(values[0].split("=")[-1])
                # if xVal>0.23:
                #  break
                xLab = str(values[0].split("=")[0])
                yVal = float(values[1].split("=")[-1])
                yLab = str(values[1].split("=")[0])
                x += [xVal]
                y += [yVal]
        for k, v in results.items():
            # print k, len(v), len(x)
            while (len(v) < len(x)):
                v += [0]
        # for k, v in results.items():
        #  print k
        #  print " v ->", v , len(v)
        #  print " x ->", x , len(x)
        #  print " y ->", y , len(y)

xBins = sorted(list(set(x)))
yBins = sorted(list(set(y)))
print("xBins", xBins)
print("yBins", yBins)
print("max_xs", max_xs)
print("min_xs", min_xs)
vals = len(xBins)*len(yBins)
arr = np.zeros(vals)
arr.resize((len(xBins), len(yBins)))
#print(len(x), len(y))
# print arr
counter = -1
plotcounter = 0
newresults = {}
# merge into pools
if opts._splitIntoPools:
    for k, v in results.items():
        MET = 1 if "\\nu" in k else 0
        jets = k.count("q")+k.count("g")-k.count("gamma")+k.count("b")
        bJets = k.count("b")
        photons = k.count("gamma")
        leptons = k.count("l")+k.count("e")+k.count("mu")+k.count("tau")
        # print k, "MET=%d, jets=%d, leptons=%d, photons=%d"%(MET, jets, leptons, photons)
        pool = "nLeptons=%d" % leptons
        if jets >= 2:
            pool += ", nJets>=2"
        if bJets > 0:
            pool += ", nBJets=%d" % bJets
        if MET:
            pool += ", MET"
        if not pool in newresults.keys():
            newresults[pool] = np.array(v)
        else:
            newresults[pool] += np.array(v)
        # print k, pool

    results = newresults


for k, v in sorted(results.items(), key=lambda x: max(x[1]), reverse=True):
    counter += 1
    print("%d/%d doing %s (max = %.3f)" % (counter, len(results), k, max(v)))
    if max(v) < opts._tolerance:
        print("skipping due to xs< tolerance !", opts._tolerance)
        continue
    heatmap = np.copy(arr)
    thisMax = max(v)
    for ig in range(len(v)):
        ix = xBins.index(x[ig])
        iy = yBins.index(y[ig])
        heatmap[ix][iy] = v[ig]
        # print x[ig], y[ig], v[ig]
    if justLog:
        fig, ax1 = plt.subplots(1, 1)
    else:
        fig, (ax0, ax1) = plt.subplots(2, 1)
    #im = ax.imshow(heatmap, norm=LogNorm(vmin=0.03, vmax=max_xs))
    #im = ax.pcolormesh(yBins, xBins,heatmap, norm=LogNorm(vmin=0.03, vmax=max_xs))
    max_xs = 10e4
    min_xs = 10e-4
    if not justLog:
        im = ax0.pcolormesh(yBins, xBins, heatmap, vmin=min_xs, vmax=max_xs)
        cs = ax0.contour(yBins, xBins, heatmap, levels=[
                         0.01, 0.1, 1, 10, 100, 1000, 10000], colors='w')
        ax0.clabel(cs, inline=1, fontsize=10, fmt="%.0e")

        ax0.set_title("$%s$" % k)
        ax0.set_ylabel(opts._xy.split(",")[1])
        cbar = ax0.figure.colorbar(im, ax=ax0)
        cbar.ax.set_ylabel("cross-section [fb]", rotation=-90, va="bottom")
    else:
        #ax1.set_title("$%s$ (max $\sigma=%f$~fb)"%(k,thisMax))
        ax1.set_title("$%s$" % (k))

    im = ax1.pcolormesh(yBins, xBins, heatmap,
                        norm=LogNorm(vmin=min_xs, vmax=max_xs))
    cs = ax1.contour(yBins, xBins, heatmap, levels=[
                     0.01, 0.1, 1, 10, 100, 1000, 10000], colors='w')
    ax1.clabel(cs, inline=1, fontsize=10, fmt="%.0e")

    # ax1.set_title(k)
    #ax1.set_ylabel("$\\kappa$")
    #ax1.set_xlabel("$m_{Q}$ [GeV]")
    ax1.set_xlabel(opts._xy.split(",")[1])
    ax1.set_ylabel(opts._xy.split(",")[0])
    #cbar = ax1.figure.colorbar(im, ax=ax1)
    #cbar.ax.set_ylabel("cross-section [fb]", rotation=-90, va="bottom")
    k = k.replace("\\rightarrow", "-->")
    plt.savefig(outdir+"/%s.png" %
                k.replace(",", "").replace("\\", "").replace(" ", "_"))
    plt.savefig(outdir+"/%s.pdf" %
                k.replace(",", "").replace("\\", "").replace(" ", "_"))
    plotcounter += 1
    cbar = plt.colorbar(im, ax=ax1)
    ax1.remove()
    cbar.ax.set_ylabel("cross-section [fb]", rotation=-90, va="bottom")
    plt.savefig(outdir+"/cbar.png", bbox_inches='tight')
    plt.savefig(outdir+"/cbar.pdf", bbox_inches='tight')
    plt.close(fig)


print("made %d plots:" % plotcounter)
