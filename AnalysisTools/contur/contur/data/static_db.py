import re
import sqlite3 as db
from os.path import dirname, join

import contur
from rivet.aopaths import AOPath

INIT = False

INVALID = (-1, '', '')
known_beams = []
pool_beam = {}
pool_desc = {}
lumis = {}
pools = {}
beams = {}
whitelists = {}
blacklists = {}
needtheory = {}
higgsgg = {}
higgsww = {}
bveto = {}
atlaswz = {}
searches = {}
metratio = {}
aLists = {}

# workaround for old global variable
global anapool
anapool = set()


class listdict(dict):
    def __missing__(self, key):
        self[key] = []
        return self[key]


subpools = listdict()
norms = listdict()
smc = listdict()


def init_dbs():
    dbfile = join(dirname(__file__), 'analyses.db')
    conn = db.connect(dbfile)
    c = conn.cursor()

    for row in c.execute('SELECT id FROM beams GROUP BY id;'):
        beam = row[0]
        known_beams.append(beam)

    for row in c.execute('SELECT pool,beam,description FROM analysis_pool;'):
        pool, beam, description = row
        pool_beam[pool] = beam
        pool_desc[pool] = description

    for row in c.execute('SELECT id,lumi,pool FROM analysis;'):

        ana, lumi, pool = row
        lumis[ana] = lumi
        if pool:
            pools[ana] = pool
            anapool.add(pool)
            beam = pool_beam.get(pool)
            beams[ana] = beam
            if not beams.get(ana) in aLists:
                aLists[beam] = ana
            else:
                aLists[beam] = aLists[beam] + " " + ana
        else:
            pools[ana] = ''
            beams[ana] = ''

    for row in c.execute('SELECT id,group_concat(pattern) FROM whitelist GROUP BY id;'):
        ana, patterns = row
        patterns = patterns.split(',')
        whitelists[ana] = patterns

    for row in c.execute('SELECT id,group_concat(pattern) FROM blacklist GROUP BY id;'):
        ana, patterns = row
        patterns = patterns.split(',')
        blacklists[ana] = patterns

    for row in c.execute('SELECT id,group_concat(pattern) FROM needtheory GROUP BY id;'):
        ana, patterns = row
        patterns = patterns.split(',')
        needtheory[ana] = patterns

    for row in c.execute('SELECT id,group_concat(pattern) FROM higgsgg GROUP BY id;'):
        ana, patterns = row
        patterns = patterns.split(',')
        higgsgg[ana] = patterns

    for row in c.execute('SELECT id,group_concat(pattern) FROM searches GROUP BY id;'):
        ana, patterns = row
        patterns = patterns.split(',')
        searches[ana] = patterns

    for row in c.execute('SELECT id,group_concat(pattern) FROM higgsww GROUP BY id;'):
        ana, patterns = row
        patterns = patterns.split(',')
        higgsww[ana] = patterns

    for row in c.execute('SELECT id,group_concat(pattern) FROM bveto GROUP BY id;'):
        ana, patterns = row
        patterns = patterns.split(',')
        bveto[ana] = patterns

    for row in c.execute('SELECT id,group_concat(pattern) FROM atlaswz GROUP BY id;'):
        ana, patterns = row
        patterns = patterns.split(',')
        atlaswz[ana] = patterns

    for row in c.execute('SELECT id,group_concat(pattern) FROM metratio GROUP BY id;'):
        ana, patterns = row
        patterns = patterns.split(',')
        metratio[ana] = patterns

    for row in c.execute('SELECT id,pattern,subid FROM subpool;'):
        ana, pattern, subid = row
        #        subid = 'R%s' % (subid + 1)
        subpools[ana].append((pattern, subid))

    for row in c.execute('SELECT id,pattern,norm,scalemc FROM normalization;'):
        ana, pattern, norm, scalemc = row
        norms[ana].append((pattern, norm))
        smc[ana].append((pattern, scalemc))

    conn.close()

    global INIT
    INIT = True


class InvalidPath(Exception):
    pass


def splitPath(path):
    m = contur.config.ANALYSISHISTO.search(path)
    if not m:
        contur.config.contur_log.debug("{} is not a valid path".format(path))
        raise InvalidPath
    else:
        analysis = m.group(1)
        tag = m.group(0).split("/")[1]
    return analysis, tag


def validHisto(h, gotTheory=True):
    """Tests a histo path to see if it is a valid contur histogram.
       Some histograms (e.g. MET ratios) are only valid if we are running in the mode where we have the SM theory for them.
       """
    if not INIT:
        init_dbs()

    try:
        ana, tag = splitPath(h)
    except InvalidPath:
        return False

    try:
        aop = AOPath(h)
        if aop.istmp() or aop.israw():
            return False
    except:
        return False

    if ana in searches and contur.config.excludeSearches:
        for pattern in searches[ana]:
            if pattern in tag:
                return False

    if ana in higgsgg and contur.config.excludeHgg:
        for pattern in higgsgg[ana]:
            if pattern in tag:
                return False

    if ana in higgsww and contur.config.excludeHWW:
        for pattern in higgsww[ana]:
            if pattern in tag:
                return False

    if ana in bveto and contur.config.excludeBVeto:
        for pattern in bveto[ana]:
            if pattern in tag:
                return False

    if ana in atlaswz and contur.config.excludeAWZ:
        for pattern in atlaswz[ana]:
            if pattern in tag:
                return False

    if isRatio(h) and contur.config.excludeMETRatio:
        return False

    if ana in lumis:
        if ana not in whitelists:
            if ana not in blacklists:
                return True
            elif ana in blacklists:
                for pattern in blacklists[ana]:
                    if pattern in tag:
                        return False
                        # else:
                        #    return True
                return True
        elif ana in whitelists:
            for pattern in whitelists[ana]:
                if pattern in tag:
                    return True
                    # else:
                    #    return False
            return False
    else:
        return False


def LumiFinder(h):
    """Get the lumi, pool and subpool for a valid contur histogram. Else return INVALID.
       """
    if not INIT:
        init_dbs()

    try:
        ana, tag = splitPath(h)
    except InvalidPath:
        return INVALID

    try:
        lumi, pool = lumis[ana], pools[ana]
        if contur.config.splitAnalysis:
            pool = tag

    except KeyError:
        return INVALID

    # tag is '' if we're looking at an R.. group
    # but R goups should not be excluded here
    if ana in whitelists and tag:
        for pattern in whitelists[ana]:
            if pattern in tag:
                break
        else:
            return INVALID

    if ana in blacklists:
        for pattern in blacklists[ana]:
            if pattern in tag:
                return INVALID

    subpool = None
    if ana in subpools:

        for p, subid in subpools[ana]:
            if re.search(p, tag):
                subpool = subid
                if contur.config.splitAnalysis:
                    pool = subid
                break

    return lumi, pool, subpool


#############################################################################################
### Special function to help with plots normalised to total xs
def isNorm(h):
    if not INIT:
        init_dbs()

    ana, tag = splitPath(h)

    isNorm = False
    normFac = 1.0
    scale_MC = 0

    # need to do it this way because for ref data ana does not have the MODE attached.
    for s in norms:
        if ana in s:
            for p, norm in norms[s]:
                if re.search(p, tag):
                    isNorm = True
                    normFac = norm
                    break

                for p, scalemc in smc[s]:
                    if re.search(p, tag):
                        scale_MC = scalemc
                        break

    return isNorm, normFac, scale_MC


################################################################################################
### Is this a ratio plot? (Can't rely on has1dhisto anymore, since some searches don't have them
def isRatio(h):
    if not INIT:
        init_dbs()

    try:
        ana, tag = splitPath(h)
    except InvalidPath:
        return False

    if ana in metratio:
        for pattern in metratio[ana]:
            if pattern in tag:
                return True

    return False


### Does this analysis have ratio measurements?
def hasRatio(ana):
    if not INIT:
        init_dbs()

    if ana in metratio:
        return True

    return False


### Does this analysis have search measurements?
def hasSearches(ana):
    if not INIT:
        init_dbs()

    if ana in searches:
        return True

    return False


### Does this analysis have measurements with a b-jet-veto problem
def hasBVeto(ana):
    if not INIT:
        init_dbs()

    if ana in bveto:
        return True

    return False


### Does this analysis have higgs to photons measurements?
def hasHiggsgg(ana):
    if not INIT:
        init_dbs()

    if ana in higgsgg:
        return True

    return False


### Does this analysis have higgs to WW measurements?
def hasHiggsWW(ana):
    if not INIT:
        init_dbs()

    if ana in higgsww:
        return True

    return False


# For some histos (e.g. the MET-JEt ratio) we always need to use the relative agreement
# vs SM, rather than the absolute.
def theoryComp(h):
    if not INIT:
        init_dbs()

    ana, tag = splitPath(h)

    if ana in needtheory:
        for pattern in needtheory[ana]:
            if pattern in tag:
                return True

    return False


def getAnalyses(pool=""):
    if not INIT:
        init_dbs()

    if pool:
        analyses = []
        for ana in pools:
            anaNoMode = ana.split(':')[0]  # remove potential mode of analysis
            if anaNoMode in analyses:  # different mode of analysis already added
                continue
            condition = (pools[ana] != pool or
                         (hasSearches(ana) and contur.config.excludeSearches) or
                         (hasHiggsgg(ana) and contur.config.excludeHgg) or
                         (hasHiggsWW(ana) and contur.config.excludeHWW) or
                         (hasBVeto(ana) and contur.config.excludeBVeto))
            if condition:
                continue
            analyses.append(anaNoMode)
        return analyses

    else:
        return aLists


def getPools():
    if not INIT:
        init_dbs()
    return pools


def getBeams(pool=False):
    if not INIT:
        init_dbs()

    if pool:
        beams = []
        expt, energy, sig = pool.split("_")
        for beam in known_beams:
            if energy in beam and (expt == "ATLAS" or expt == "CMS" or expt == "LHCb" or expt == "ALICE"):
                return [beam]
        contur.config.contur_log.error("No beam found for pool {}".format(pool))
    else:
        return known_beams


def getDescription(pool):
    if not INIT:
        init_dbs()
    return pool_desc[pool]
