
import contur
import numpy as np
import scipy.stats as spstat
from numpy import linalg as la
from scipy.optimize import minimize


class Bucket(object):
    """Class holding observables and predictions and the related correlation of these observables, automatically calculates a series of statistical metrics

        :param s:
            Signal counts (*list or single*)
        :type s: ``float``
        :param bg:
            Background counts (*list or single*)
        :type bg: ``float``
        :param nobs:
            Observed counts (*list or single*)
        :type nobs: ``float``
        :param serr:
            Signal error (*list or single*) - symmetrized as assumed symmetrical statistical error
        :type serr: ``float``
        :param cov:
            Covariance matrix for the observed counts
        :type cov: :mod:`numpy.array`
        :param uncov:
            Uncorrelated covariance matrix for the observed data
        :type uncov: :mod:`numpy.array`

        :param \**kwargs:
            See below

        :Keyword Arguments:
            * *thcov* (:mod:`numpy.array`) --
              Covariance matrix for the background counts
            * *thuncov* (:mod:`numpy.array`) --
              Uncorrelated covariance matrix for the background counts
            * *ratio* (``bool``) --
              Mark true if the data is derived from a ratio, hence signal represents s+b
            * *useTheory* (``bool``) --
              Mark true to control using theory errors

    """

     #def __init__(self, s, bg, nobs, serr, bgerr, nobserr, cov, uncov, theorycov=None, theoryuncov=None, nuisErrs=None,
      #            thErrs=None,
       #           ratio=False, useTheory=False, lumi=1.0, tags=''):
    def __init__(self, s, bg, nobs, serr, cov, uncov, theorycov=None, theoryuncov=None, nuisErrs=None,
                 thErrs=None,
                 ratio=False, useTheory=False, lumi=1.0, tags=''):

        # First the pieces from the covariance tool
        self._cov = cov
        self._uncov = uncov
        self._nuisErrs = nuisErrs
        self._thcov = theorycov
        self._thuncov = theoryuncov
        if (thErrs is not None) and (nuisErrs is not None):
            # combined theory uncertainties to give one big list of errors. TODO check for key name overlap.
            self._nuisErrs.update(thErrs)
        self._useTheory = useTheory
        self._lumi = lumi

        self.ratio = ratio

        # now the vectors of observables
        self._bg = np.asarray(bg)
        self._s = np.asarray(s)
        self._n = nobs
        self._nbins = len(self._s)

        # signal unncertainty vector. (the bg and meas are taken from the cov matrices)
        self._serr = np.asarray(serr)

        # lastly the members we want to work out
        self._covinv = None
        self._sumTS = None
        self._pval = None
        self._CLs = 0.0
        # The p val and CLs are representations of a given bucket but to combine the buckets it is worth keeping the test statistics, as once these are available they can be added naively
        self._ts_s_b = None
        self._ts_b = None

        self._p_s_b = None
        self._p_b = None

        self._index = None

        # A bool to work out whether the correlations should be taken or not
        self._covBuilt = False

        self._tags = tags
        # these are set later.
        self._pools = ''
        self._subpools = ''

        # only do this stuff if there are actually any signal events!
        if self._s.any():
            self.__buildCovinv()
            self.__pval()

    def __removeTheory(self):
        # Cheap function to quickly swap out the bg prediction if we have asked not to use theory
        self._bg = self._n

    def __pval(self):
        """Internal function to trigger the calculation of the various statistical metrics on initialisation.
        """
        self._ts_s_b, self._ts_b = self.__chisq(mu_test=1.0, mu_null=0.0)

        # if either test statistic is 0 something went wrong, probably no correlation found, it's probably never negative but check anyway
        if self._ts_s_b is not None:
            self._pval = self.__ts_to_pval(
                self._ts_s_b) / (1 - self.__ts_to_pval(self._ts_b))
            self._CLs = self.__ts_ts_to_cls(self._ts_s_b, self._ts_b)
            self._p_s_b = self.__ts_to_pval(self._ts_s_b)
            self._p_b = self.__ts_to_pval(self._ts_b)
        else:
            self._pval = None
            self._CLs = None

        if self._CLs is not None:
            contur.config.contur_log.debug("CLs %f, pval %f, p_sb %f, p_b %f, ts_s_b %f, ts_b %f:" % (
                self._CLs, self._pval, self._p_s_b, self._p_b, self._ts_s_b, self._ts_b))

            if np.isnan(self._CLs):
                self._CLs = None
                contur.config.contur_log.warning(
                    "CLs evaluated to nan, set to None. " + self._tags)
        else:
            contur.config.contur_log.warning(
                "Could not evaluate CLs for" + self._tags)

    def __ts_ts_to_cls(self, ts_s_b, ts_b):
        """Internal function to directly cast a test statistic into a CLs value

        :arg ts_s_b: test statistic of signal hypothesis
        :type ts_s_b: ``float``
        :arg ts_b: test statistic of background hypothesis
        :type ts_b: ``float``

        :return: CLs ``float`` -- Confidence Interval in CLs formalism

        """

        pval_b = self.__ts_to_pval(ts_b)
        pval_sb = self.__ts_to_pval(ts_s_b)
        if pval_b <= 0:
            if pval_sb <= 0:
                contur.config.contur_log.warning(
                    "Zero consistency with SM. Unable to evaluate exclusion. " + self._tags)
                cls = None
            else:
                # s+b agrees better than b; truncate cls at zero.
                contur.config.contur_log.warning(
                    "Zero consistency with SM. BSM+SM is in better agreement with data. " + self._tags)
                cls = 0
        else:
            cls = 1 - pval_sb / pval_b
            contur.config.contur_log.debug(
                "CLs %e, pval sb %e, pval b %e:" % (cls, self.__ts_to_pval(ts_s_b), self.__ts_to_pval(ts_b)))

        if (cls is not None and cls < 0):
            contur.config.contur_log.warning(
                "Negative CLs %f, setting to zero for %s. BSM+SM is in better agreement with data." % (cls, self._tags))
            cls = 0

        return cls

    def __ts_to_pval(self, ts):
        """Internal function to convert test statistic to pval

        :arg ts: test statistic to convert to a p-value with a Gaussian
        :type ts: ``float``

        :return: p-value ``float``

        """
        return spstat.norm.sf(np.sqrt(ts))

    def __statCov(self):
        """Internal function to work out the statistical errors on the signal MC generation so it can be added to the covariance
        """
        self.statErrs = np.zeros(self._nbins)
        for ibin in range(self._nbins):
            self.statErrs[ibin] = (self._serr[ibin])

    def __addthCov(self):
        """Internal function to add the theory covariance to the data derived one in most foreseeable cases the annotations will not exist 
        on what is used for theory so these will always be uncorrelated. Fetches and returns this thing
        """
        if contur.config.useTheoryCorr and self._thcov is not None:
            if self._cov is not None:
                self._cov += self._thcov
            if self._uncov is not None:
                # if we don't have data correlations, we use the single bin method so add uncorrelated theory uncertainties.
                self._uncov += self._thuncov
            else:
                contur.config.contur_log.error(
                    "Could not figure out how to combine theory and refdata uncertainties")
        elif self._thuncov is not None:
            if self._cov is not None:
                self._cov += self._thuncov
            elif self._uncov is not None:
                self._uncov += self._thuncov
            else:
                contur.config.contur_log.error(
                    "Could not figure out how to combine theory and refdata uncertainties")

    def __buildCovinv(self):
        """Internal function to take the covariance matrix and invert it for the chi square test

        In building the Cov matrix we note three cases
            * The matrix is build and has det!=0, the variables have been built with a covariance that makes sense and the nusiances can be correlated accordingly

            * The matrix has det==0, so we use a construction that diagonalises the covariance with the sum of the errors, so from now on if covinv==diagonal, we treat each bin as a separate test

            * The diagonalised matrix also has det==0, this means one of the diagonals has 0 uncertainty, this bin could be removed in construction but for now it is considered pathological so the whole entry is discarded

        """
        self.__statCov()
        if contur.config.useTheory or self._useTheory:
            self.__addthCov()
        statCov = np.diag(self.statErrs * self.statErrs)

        # we also test on whether enough nuisance errors are material (above the threshold)
        if self._cov is not None and len(self._nuisErrs) >= contur.config.min_num_sys:
            if la.det(self._cov):
                self._covinv = la.inv(self._cov + statCov)
                self._covBuilt = True
        elif self._uncov is not None:
            if contur.config.buildCorr:
                if self._nuisErrs is not None:
                    n_nuis = len(self._nuisErrs)
                else:
                    n_nuis = 0
                contur.config.contur_log.debug(
                    "Actually not using correlations for {}. Only {} available".format(self._tags, n_nuis))
            diagonal = np.diag(self._uncov + statCov)
            # this is diagonal anyway so do quicker & more reliably
            if np.count_nonzero(diagonal) == len(diagonal):
                self._covinv = np.diag(1.0 / (diagonal))
            else:
                contur.config.contur_log.warning(
                    self._tags + " has at least one element with zero uncertainty. Can't invert. Data discarded.")

            contur.config.contur_log.debug(
                "syst err element (0,0) = {}, stat err={}".format(np.sqrt(self._uncov[0][0]), np.sqrt(statCov[0][0])))
            self._covBuilt = False
        else:
            contur.config.contur_log.warning(
                "Couldn't invert covariance matrix for " + self._tags + ". Data discarded.")

    def __chisq(self, mu_test, mu_null):
        """Internal function to calculate a pair of chi square test statistics corresponding to two input hypothesis values.

        :arg mu_test:
            The signal strength to test
        :type mu_test: ``float``
        :arg mu_null:
            The signal strength in the null hypothesis
        :type mu_null: ``float``

        :Requirements:
            * :func:`self.__buildCovinv` runs and populated `_covinv` and `_covBuilt` exist

        :return: (ts_tested,ts_null) ``float`` -- Returns a tuple of the test statistics corresponding to the requested signal strength parameters

        """
        if self._covinv is not None:
            sb_offset = np.zeros(self._nbins)
            b_offset = np.zeros(self._nbins)
            b_nuisance = None

            if self._covBuilt:

                sb_nuisance = self.__minimise_LL(1.0)

                if sb_nuisance is not None:

                    if contur.config.useTheory or self.ratio:
                        # these will be zero by definition unless we have a theory prediction different from data.
                        b_nuisance = self.__minimise_LL(0.0)

                    for errorSource, error in self._nuisErrs.items():
                        try:
                            sb_offset += error * sb_nuisance[errorSource]
                        except KeyError:
                            contur.config.contur_log.warning(
                                "Missing errorSource " + errorSource + " in " + self._tags)
                        if b_nuisance is not None:
                            b_offset += error * b_nuisance[errorSource]
                        contur.config.contur_log.debug(
                            "Nuisance " + errorSource + " for " + self._tags + ":" + str(sb_nuisance[errorSource]))

                    contur.config.contur_log.debug(
                        "Offsets for " + self._tags + ":" + str(sb_offset))

            if self.ratio:
                # if the source is a ratio then the generated "signal" corresponds to s+b
                delta_mu_test = (self._s + sb_offset) - self._n
                # for now lets just assume mu_null is always 0 in this case, which in practise it will be
                delta_mu_null = (self._bg + b_offset) - self._n
            else:
                delta_mu_test = (mu_test * self._s +
                                 self._bg + sb_offset) - self._n
                delta_mu_null = (mu_null * self._s +
                                 self._bg + b_offset) - self._n

            if self._covBuilt and sb_nuisance is not None:
                return np.dot(delta_mu_test, np.dot(self._covinv, delta_mu_test)), np.dot(delta_mu_null,
                                                                                          np.dot(self._covinv,
                                                                                                 delta_mu_null))
            else:
                # treating errors as uncorrelated.
                # do some magic to find the max single
                ts_ts = zip([(x ** 2) * y for x, y in zip(delta_mu_test, np.diag(self._covinv))],
                            [(x ** 2) * y for x, y in zip(delta_mu_null, np.diag(self._covinv))])
                cls = [(self._Bucket__ts_ts_to_cls(
                    x, y),x,y) for x, y in ts_ts]

                self._index = cls.index(max(cls)) + 1

                contur.config.contur_log.debug(
                    "n data, {} signal, {} background {}, bin {} ".format(self._n, self._s, self._bg, self._index))

                x=max(cls, key=lambda item: item[0])
                return x[1],x[2] #ts_ts[cls.index(max(cls))]
        else:
            return None, None

    def __minimise_LL(self, mu_test):
        """ Function to find the nuisance parameters which minimise the log likilood for (mu_test*s + b) 
        Use self._nuisErrs, which is dictionary of the syst uncertainties(bin) keyed by the [errorSource].
        Will also need self._s and self_.bg
        Returns None if the minimization fails.
        """

        def evalNLL(v, mu, s, b, serr, n, sigma, poissonOnly=False):
            LL = 0
            gaussianFactorL = 0

            for val in v:
                gaussianFactorL += spstat.norm.logpdf(val, 0., 1.)

            for i in range(len(n)):

                source = 0
                offset = 0
                for errorVal in sigma[i]:
                    offset = offset + v[source] * errorVal
                    source = source + 1

                poissonFactorL = spstat.poisson.logpmf(
                    n[i], mu * s[i] + b[i] + offset)
                if np.isnan(poissonFactorL):
                    # will happen for empty bins, but seems harmless.
                    contur.config.contur_log.debug(
                        "Poisson returned nan for n=%f,mu=%f,s=%f,b=%f" % (n[i], mu, s[i], b[i]))
                else:
                    LL += poissonFactorL

            if not poissonOnly:
                LL += gaussianFactorL

                # print "-LL ",-1*LL, gaussianFactorL, poissonFactorL, v
            return -1 * LL

        if self._nuisErrs is not None:
            nuisance = {}
            mu = mu_test  # signal strength

            s = []
            b = []
            n = []
            serr = []
            sigma = []

            for i in range(len(self._n)):
                # Poisson expects the observed to be an integer...
                n.append(int(round(self._lumi * self._n[i])))
                # Round the others too but keep them floats
                b.append(round(self._lumi * self._bg[i]))
                # JMB I am worried that we don't seem to take the MC statistical uncertainty into account here,
                #    and to do so seems from the literature to be prohibitively complex and slow. See
                #    R. Barlow and C. Beeston, Comp. Phys. Comm. 77 (1993) 219.
                #    J. S. Conway in Proceedings of PHYSTAT 2011, https://cds.cern.ch/record/1306523
                #    So for now we will not include bins with very low numbers of MC events (<4) in the
                #    nuisance parameter fitting, otherwise they can move the nuisance parameters
                #    too far from nominal, leading to unrealistically strong exclusion in the final
                #    result (even though MC stats are taken into account in the error matrix).
                tmp_s = round(self._lumi * self._s[i])
                tmp_serr = self._lumi * self._serr[i]
                if tmp_s > 0:
                    nmc = tmp_s * tmp_s / (tmp_serr * tmp_serr)
                    if nmc < 4:
                        tmp_s = 0
                # This avoid low-stats fitting to rounding errors...
                s.append(tmp_s)
                serr.append(tmp_serr)

            for errorSource in self._nuisErrs:
                sigma.append(self._lumi * self._nuisErrs[errorSource])

            v0 = np.zeros(len(self._nuisErrs))

            s = np.array(s)
            n = np.array(n)
            b = np.array(b)
            serr = np.array(serr)
            sigma = np.transpose(np.array(sigma))

            contur.config.contur_log.debug(
                "%s , Using minimization method to find best-fit nuisances" % (self._tags))
            minimizationSucceeded = False
            try:
                res = minimize(evalNLL, v0, args=(mu, s, b, serr, n, sigma), method='Nelder-Mead',
                               options={'disp': False, 'fatol': contur.config.ll_prec, 'xatol': contur.config.err_prec,
                                        'adaptive': True, 'maxiter': contur.config.n_iter*len(v0)}, )  # minimize quietly.
                if res.success:
                    nuisance_values = res.x
                    minimizationSucceeded = True
                else:
                    contur.config.contur_log.warning(
                        "Minimization did not converge " + self._tags)

            except Exception as e:
                contur.config.contur_log.warning(
                    "Minimization crashed " + self._tags, exc_info=e)
                contur.config.contur_log.warning("v0 {}, nuisances {}, len {}").format(v0, self._nuisErrs,
                                                                                       len(self._nuisErrs))

            if (minimizationSucceeded == False):
                contur.config.contur_log.warning(
                    "Minimization for %s, mu=%.2f has failed ! Will ignore correlations" % (self._tags, mu_test))
                nuisance = None
            else:
                i = 0
                for source in self._nuisErrs:
                    nuisance[source] = nuisance_values[i]
                    i += 1

            return nuisance

        else:
            return None

    @property
    def tags(self):
        """Analysis tag defining the specific collection of observables in this bucket, typically a histogram ID

        :return: Histogram tag ``string`` -- specifically a rivet.AOPath
        *settable parameter*
        """
        return self._tags

    @tags.setter
    def tags(self, value):
        self._tags = value

    @property
    def pools(self):
        """Analysis Pool, defining statistically correlated (overlapping datasets) groupings of analyses

        :return: Analysis pools ``string``
        *settable parameter*
        """
        return self._pools

    @pools.setter
    def pools(self, value):
        self._pools = value

    @property
    def subpools(self):
        """An additional tag to identify subgrouping available with a pool, primarily to additionally uncorrelate histograms

        :return: Analysis subpools ``string``
        *settable parameter*
        """
        return self._subpools

    @subpools.setter
    def subpools(self, value):
        self._subpools = value

    @property
    def p_s_b(self):
        """p-value of s+b hypothesis

        :return: p_s_b ``float``
        """
        return self._p_s_b

    @property
    def p_b(self):
        """p-value of b only hypothesis

        :return: p_b ``float``
        """
        return self._p_b

    @property
    def index(self):
        """Index of max bin, if running without correlations, otherwise returns ``None``

        :return: index ``int``
        """
        return self._index

    @property
    def CLs(self):
        """CLs value defined as ratio of ``p_s_b`` and ``1-p_b``

        :return: CLs ``float`` -- The Confidence interval calculated in CLs formalism
        """
        return self._CLs

    @CLs.setter
    def CLs(self, value):
        self._CLs = value

    @property
    def ts_s_b(self):
        """The constructed test statistic of the s+b hypothesis

        :return: ts ``float``
        *settable parameter*
        """
        return self._ts_s_b

    @ts_s_b.setter
    def ts_s_b(self, value):
        self._ts_s_b = value

    @property
    def ts_b(self):
        """The constructed test statistic of the bg only hypothesis

        :return: ts ``float``
        *settable parameter*
        """
        return self._ts_b

    @ts_b.setter
    def ts_b(self, value):
        self._ts_b = value

    def __repr__(self):
        if not self.tags:
            tag = "Unidentified Source"
        else:
            tag = self.pools + self.tags
        return "%s from %s, CLs=%.2f" % (self.__class__.__name__, tag, self.CLs)


class CombinedBucket(Bucket):
    """A placeholder to steal the test statistic calculator from conturBucket to add the buckets"""

    def __init__(self):
        # super(self.__class__, self).__init__()
        self.ts_s_b = 0.0
        self.ts_b = 0.0
        self._tags = ''
        self._pools = ''
        self._subpools = ''

    def calcCLs(self):
        """Function to call the CLs calculation on the current test statistic values

        """
        self.CLs = self._Bucket__ts_ts_to_cls(self.ts_s_b, self.ts_b)

    def addTS(self, conturBucket):
        """Function to add the statistics from a conturBucket to the BucketBucket.

        :arg conturBucket: instance to add
        :type conturBucket: :class:`contur.conturBucket`
        """
        if conturBucket.ts_b is not None and conturBucket.ts_s_b is not None:
            self.ts_s_b += conturBucket.ts_s_b
            self.ts_b += conturBucket.ts_b

    def __repr__(self):
        if not self.tags:
            tag = "Combined buckets"
        else:
            tag = self.pools + self.tags
        return "%s from %s, CLs=%.2f" % (self.__class__.__name__, tag, self.CLs)
