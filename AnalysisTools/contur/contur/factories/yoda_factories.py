import os
import re
import sys

import contur
import rivet
import yoda

# TODO: move this wrapper to a common contur util area
try:
    from tqdm import tqdm as pbar
except ImportError:
    def pbar(iterable, **kwargs):
        return iterable

# TODO replace the regex matching with the rivet libary option rivet.AOPath(path).basepathparts(keepref=False)


refObj = {}
thyObj = {}
refCorr = {}
refUncorr = {}
refErrors = {}
theoryCorr = {}
theoryUncorr = {}
theoryErrors = {}

# A one off to hold objects that have been scaled in an unscaled form for plotting
plotObj = {}
plotThObj = {}


def init_ref(aopath=[]):
    """Function to load all reference data and theory *.yoda data

        :arg aopath:
            List of rivet.AOPath ID's to load, default empty list loads everything in $RIVET_REF_PATH
        :type aopath: ``list`` of ``string``

    """

    refFiles = []

    refObj.clear()
    refCorr.clear()
    refUncorr.clear()
    refErrors.clear()

    thyObj.clear()
    theoryCorr.clear()
    theoryUncorr.clear()
    theoryErrors.clear()

    if contur.config.onlyAnalyses:
        temp = []
        for only in contur.config.onlyAnalyses:
            temp.extend([x for x in aopath if only in rivet.stripOptions(x)])

        aopath = temp
    if contur.config.vetoAnalyses:
        temp = []
        for x in aopath:
            vetoed = False
            for veto in contur.config.vetoAnalyses:
                if veto in rivet.stripOptions(x):
                    vetoed = True
            if not vetoed:
                temp.append(x)
        aopath = temp

        # check if the list is now null to exit early
    if not aopath:
        contur.config.contur_log.critical(
            "Choice of veto or only analyses left list of signal yoda empty, try different veto arguments")
        sys.exit(1)

    contur.config.contur_log.info(
        "Loading reference and theory data from all yoda files in $RIVET_DATA_PATH matching paths in input yoda")
    rivet_data_dirs = rivet.getAnalysisRefPaths()
    rivet_plot_dirs = rivet.getAnalysisPlotPaths()
    global plotInfo
    plotInfo = rivet.mkStdPlotParser(rivet_plot_dirs)
    for dirs in rivet_data_dirs:
        import glob
        refFiles.append(glob.glob(os.path.join(dirs, '*.yoda')))
        refFiles.append(glob.glob(os.path.join(dirs, '*.yoda.gz')))
    for fileList in refFiles:
        for f in pbar(fileList, desc="Processing reference/theory YODAs"):
            # if contur.fastMode and aopath:
            if aopath:
                # An even more extreme version of the speedup, only read the file in the first place if it matches the analysis string
                # where the acceptable strings are taken from the signal yoda
                match = contur.config.ANALYSISPATTERN.search(f)
                if match:
                    if not any([match.group() in x for x in set(
                            [contur.config.ANALYSISPATTERN.search(x).group() for x in aopath if
                             contur.config.ANALYSISPATTERN.search(x)])]):
                        continue
                else:
                    continue

            aos = yoda.read(f)
            for path, ao in aos.items():
                # since Rivet3 we now book RAW histos, get rid of these to speed up this loop
                if not (rivet.isRefPath(path) or rivet.isTheoryPath(path)):
                    continue
                if ao.type() == "Scatter3D":
                    continue
                if aopath:  # and contur.fastMode:
                    # To save time, only load histograms that appear explictly in the signal yoda paths
                    # This breaks multi-beam grid-mode running so turned off by default.
                    # Could still save some time by having a list of experiments to search for.
                    match = contur.config.ANALYSISHISTO.search(
                        rivet.stripOptions(path))
                    if match:
                        if not any([match.group() in x for x in [rivet.stripOptions(x) for x in aopath]]):
                            continue
                    else:
                        continue
                # otherwise everything is fair game, but this is slower
                if ao.type() != "Scatter2D":
                    ao = yoda.mkScatter(ao)
                if ao.type() == "Scatter1D":
                    ao = contur.util.mkScatter2D(ao)

                _isScaled, _scaleFactorData, _scaleMC = contur.data.isNorm(ao.path())
                # build the covariance object to fill the dictionaries
                c = contur.data.CovarianceBuilder(ao)

                if rivet.isRefPath(path):

                    if _isScaled:
                        if not contur.config.gridMode:
                            plotObj[path] = ao.clone()
                        scale_scatter_2d(ao, _scaleFactorData)
                    refObj[path] = ao
                    # always fill the unCorr case in case we need it later
                    refUncorr[path] = c.buildCovFromErrorBar(
                        assume_correlated=False)
                    if c.hasBreakdown and contur.config.buildCorr:
                        refCorr[path] = c.buildCovFromBreakdown(
                            ignore_corrs=False)
                        refErrors[path] = c.getErrorBreakdown()
                        # print("{} REF errors {}".format(path,refErrors[path]))

                        # NB don't need to scale the errors again because they were already scaled in the "scale_scatter" step.

                if rivet.isTheoryPath(path):

                    if _isScaled:
                        if not contur.config.gridMode:
                            plotThObj[path] = ao.clone()
                        scale_scatter_2d(ao, _scaleFactorData)
                    thyObj[path] = ao

                    if c.hasBreakdown and contur.config.buildCorr:
                        if contur.config.useTheoryCorr:
                            theoryCorr[path] = c.buildCovFromBreakdown(
                                ignore_corrs=False)
                        # always fill the unCorr case in case we need it later
                        theoryUncorr[path] = c.buildCovFromBreakdown(
                            ignore_corrs=True)

                    else:
                        if contur.config.useTheoryCorr:
                            theoryCorr[path] = c.buildCovFromErrorBar(
                                assume_correlated=True)
                        theoryUncorr[path] = c.buildCovFromErrorBar(
                            assume_correlated=False)

                    # NB don't need to scale the errors again because they were already scaled in the "scale_scatter" step.
                    theoryErrors[path] = c.getErrorBreakdown()

    contur.config.contur_log.info("Done loading static data")
    # global REFLOADED
    contur.config.refLoaded = True
    return aopath


def scale_scatter_2d(ao, sf):
    # NB: only scales the error breakdown if we are using correlations, to save time.
    for i in range(0, len(ao.points())):
        ao.points()[i].setY(ao.points()[i].y() * sf)
        contur.config.contur_log.debug(
            "Scaling {}: {} SF={}".format(ao.path(),ao.points()[i].yErrs(), sf))
        if contur.config.buildCorr and ao.hasValidErrorBreakdown():
            for source in ao.variations():
                ao.points()[i].setErrMinus(2,ao.points()[i].errMap()[source][0]*sf, source)
                ao.points()[i].setErrPlus(2,ao.points()[i].errMap()[source][1]*sf, source)
        else:
            ao.points()[i].setYErrs(
                map(lambda x: x * sf, ao.points()[i].yErrs()))

        contur.config.contur_log.debug("Scaled: {}".format(ao.points()[i].yErrs()))


class HistFactory(object):
    """Processes and decorates :class:`YODA.AnalysisObjects` to a testable format, filling a candidate conturBucket by default

        :param ana_obj:
            YODA aos to dress
        :type ana_obj: :class:`YODA.AnalysisObject`
        :param xsec:
            _XSEC scatter recording generator cross section in YODA file (*contained in all Rivet run outputs*)
        :type xsec: :class:`YODA.Scatter1D`
        :param nev:
            _EVTCOUNT scatter recording total generated events in YODA file (*contained in all Rivet run outputs*)
        :type nev: :class:`YODA.Scatter1D`

        :Built members:
            * *conturBucket* (:class:`contur.conturBucket`) --
              Automatically filled bucket containing statistical test pertaining to this histogram
            * Various plotting members

        :Keyword Arguments:
            * *aopath* (``list``) --
              List of rivet.AOPath ID's to load, default empty list loads everything in $RIVET_REF_PATH

    """

    def __init__(self, ana_obj, xsec, nev, aopaths=None):

        # Construct with an input yoda aos and a scatter1D for the cross section and nEv
        self.signal = ana_obj
        self.xsec = xsec
        self.nev = nev
        self.aopaths = aopaths

        self._ref = None

        self._weight = rivet.extractWeightName(self.signal.path())
        if self._weight != contur.config.weight:
            return

        self.signal.setPath(rivet.stripWeightName(self.signal.path()))

        # Initialize the public members we always want to access
        self._CLs = None
        self._IDstring = ''
        self._has1Dhisto = None
        self._isRatio = contur.data.isRatio(ana_obj.path())
        self._background = None
        self._stack = yoda.Scatter2D

        self._refplot = None
        self._sigplot = None
        self._bgplot = None

        self._lumi = 1
        self._isScaled = False
        self._scaleFactorData = 1
        self._scaleFactorSig = 1
        self._conturPoints = []
        self._scaleMC = 1.0
        self._maxcl = -1
        self._maxbin = -1
        self._cov = None
        self._uncov = None
        self._nuisErrs = None
        self._bucket = None

        self._gotTh = False
        self._thyplot = None
        self._thCov = None
        self._thUncov = None
        self._thErrs = None

        # Call the internal functions on initialization
        # to fill the above members with what we want, these should all be private


        # Get the measurement reference data. 
        if not self.__getData():
            return

        # Get the theory reference data. (Do this even if not using it, as it is useful for reference.)
        self.__getThy()
        if (contur.config.theoryOnly or contur.config.expectedLimit) and not self._gotTh:
            # can't carry on with these options if there is no theory prediction available.
            return

        # this is the place to swap in or out data and theory.

        # If no background has been assigned, make it the REF data  
        if self._ref is not None and self._background is None:
            self._background = self._ref.clone()

        if contur.config.expectedLimit:
            # copy the SM theory to use it as the measurement too. 
            self.__theoryToRef()

        self.__getAux()
        self.__getisScaled()

        # Determine the type of object we have, and build a 2D scatter from it if it is not one already
        # Also recalculate scalefactor, if appropriate
        if self.signal.type() == 'Histo1D' or self.signal.type() == 'Profile1D' or self.signal.type() == 'Counter':

            self._has1Dhisto = True

            if self._isScaled:
                # if the plot is area normalised (ie scaled), work out the factor from number of events and generator xs
                # (this is just the integrated cross section associated with the plot)
                try:
                    self._scaleFactorSig = (
                        float(self.xsec.points()[0].x())) * float(
                        self.signal.numEntries()) / float(
                        self.nev.numEntries())

                except Exception as e:
                    contur.config.contur_log.warning(
                        "missing info for scalefactor calc", exc_info=e)

            self.signal = yoda.mkScatter(self.signal)
            # Make sure it is actually a Scatter2D - mkScatter makes Scatter1D from counter.
            if self.signal.type() == 'Scatter1D':
                self.signal = contur.util.mkScatter2D(self.signal)

        if not contur.config.gridMode:
            # Public member function to build plots needed for direct histogram visualisation
            # avoid calling YODA.clone() unless we have to
            # Must be called before scaling.
            contur.config.contur_log.info("Making raw Rivet plots for visualisation")
            if self._ref:
                self.doPlot()
            else:
                contur.config.contur_log.warning(
                    "No reference data found for histo: %s" % self.signal.path())



        # if everything we need is available, there will be ref data.
        if self._ref:

            # don't scale histograms that came in as 2D scatters
            if self._has1Dhisto and self._isScaled:
                self.__doScale()
            self.__fillBucket()

    def __getisScaled(self):
        """Internal function to look up Scaling attributes from the contur database, defined in :mod:`contur.stat`

        :Built members:
            * *isScaled* (``bool``) --
              True if some scaling has been applied to this histogram
            * *scaleFactorData* (``float``) --
              Factor to scale the ref data by (n count) to undo the normalisation

        """
        self._isScaled, self._scaleFactorData, self._scaleMC = contur.data.isNorm(
            self.signal.path())

    def __getData(self):
        """Internal function to look up the refdata, triggers a call to :func:`contur.histFactory.init_ref`, global REFLOADED flag to check if this has already been run

        :Built members:
            * *ref* (:class:YODA.Scatter2D) --
              Reference scatter plot matching path from input signal aos
            * *nuisErrs* (:class:`numpy.array`) --
              Uncertainty contributions on the reference data
            * *cov* (:class:`numpy.array`) --
              Built covariance matrix from ref annotations
            * *uncov* (:class:`numpy.array`) --
              Built covariance matrix from ref annotations assuming all uncertainty is fully uncorrelated

        """

        if not contur.config.refLoaded:
            self.aopaths = init_ref(self.aopaths)

        try:
            self._ref = refObj["/REF" + rivet.stripOptions(self.signal.path())]
        except:
            p = re.compile(rivet.stripOptions(self.signal.path()))
            if filter(p.match, refObj.keys()):
                # we should use the regex match + ref but to fast lets use the old iteration
                for path, ao in refObj.items():
                    if self.signal.path() in path and rivet.isRefPath(path):
                        self._ref = ao.clone()

        if self._ref is None:
            return False

        try:
            self._cov = refCorr["/REF" +
                                rivet.stripOptions(self.signal.path())]
            self._nuisErrs = refErrors["/REF" +
                                       rivet.stripOptions(self.signal.path())]
            contur.config.contur_log.debug(
                "Attempting to use correlation information for %s" % self.signal.path())
        except:
            contur.config.contur_log.debug(
                "No correlation information for %s" % self.signal.path())
            self._cov = None
            self._nuisErrs = None
        try:
            self._uncov = refUncorr["/REF" +
                                    rivet.stripOptions(self.signal.path())]
        except:
            self._uncov = None
            return False
        return True


    def __theoryToRef(self):
        """Internal function to replace the refdata (central values only) with the theory 

        :Modified members:
            * *ref* (:class:YODA.Scatter2D) --
              Reference scatter plot matching path from input signal aos

        """

        # can't do this if we don't have a theory prediction!
        if not self._gotTh:
            return

        for i in range(0, len(self._ref.points())):            
            self._ref.points()[i].setY(self._thyplot.points()[i].y())
            try:
                plotObj["/REF" + rivet.stripOptions(self.signal.path())].points()[i].setY(
                    plotThObj["/THY" + rivet.stripOptions(self.signal.path())].points()[i].y())
            except KeyError:
                # no problem, this just means the plot is unscaled.
                continue
                
    def __getThy(self):
        """Internal function to look up the theory data, triggers a call to :func:`contur.histFactory.init_ref`, global REFLOADED flag to check if this has already been run

        :Built members:
            * *thyplot* (:class:YODA.Scatter2D) --
              Theory scatter plot matching path from input signal aos
            * *thCov* (:class:`numpy.array`) --
              Built covariance matrix from thy annotations
            * *thUncov* (:class:`numpy.array`) --
              Built covariance matrix from thy annotations assuming all uncertainty is fully uncorrelated
            * *thErrs* (:class:`numpy.array`) --
              Uncertainty contributions on the theory data
            * *background* (:class:YODA.Scatter2D) --
              Background model, either built from the ref or thy if found and want to use

        """
        if not contur.config.refLoaded:
            self.aopaths = init_ref(self.aopaths)

        # find whether theory is always required for this histogram
        self._theoryComp = contur.data.theoryComp(self.signal.path())

        try:
            self._thyplot = thyObj["/THY" +
                                   rivet.stripOptions(self.signal.path())]
            self._gotTh = True

            if self._theoryComp or contur.config.useTheory or contur.config.expectedLimit:
                self._background = self._thyplot.clone()

                try:
                    self._thCov = theoryCorr["/THY" +
                                             rivet.stripOptions(self.signal.path())]
                    self._thErrs = theoryErrors["/THY" +
                                                rivet.stripOptions(self.signal.path())]
                except:
                    self._thCov = None
                    self._thErrs = None
                try:
                    self._thUncov = theoryUncorr["/THY" +
                                                 rivet.stripOptions(self.signal.path())]
                except KeyError:
                    self._thUncov = None
                    # just warn if we can't build theory, it's less important...
                    contur.config.contur_log.warning(
                        "Could not build any theory error source for %s" % self.signal.path())

                contur.config.contur_log.debug(
                    "Using theory for %s" % self._thyplot.path())
        except KeyError:
            # No theory for this one
            self._gotTh = False

    def doPlot(self):
        """Public member function to build yoda plot members for interactive runs"""
        # see if there are unscaled versions of the histos
        try:
            self._refplot = plotObj["/REF" +
                                    rivet.stripOptions(self.signal.path())]
        # otherwise the standard ref should be unscaled
        except:
            self._refplot = self._ref.clone()

        # and the same thought process for the background model, and for the theory (even if the
        # theory is not being used as background).
        try:
            if self._theoryComp or contur.config.useTheory or contur.config.expectedLimit:
                self._bgplot = plotThObj["/THY" +
                                         rivet.stripOptions(self.signal.path())]
            else:
                self._bgplot = self._refplot.clone()
            self._thyplot = plotThObj["/THY" +
                                      rivet.stripOptions(self.signal.path())]
        except:
            if not self._gotTh:
                self._bgplot = self._refplot.clone()
            else:
                self._bgplot = self._background.clone()

        # build stack for plotting, for histogrammed data
        if not self._isRatio:
            self.__buildStack()
        else:
            self._stack = self.signal.clone()
        self._sigplot = self.signal.clone()

    def __getAux(self):
        """Internal function to look up auxiliary attributes from the contur database, defined in :mod:`contur.stat`

        :Built members:
            * *pool* (``string``) --
              String for analysis pool looked up from contur database
            * *subpool* (``string``) --
              String for analysis subpool looked up from contur database

        """
        self._lumi, self.pool, self.subpool = contur.data.LumiFinder(
            self.signal.path())

    def __buildStack(self):
        """Private function to stack the signal for easier visualisation

        """

        if self.signal.type() != "Scatter2D":
            return False
        elif not self._bgplot:
            return False
        else:
            self._stack = self.signal.clone()
            if self._stack.numPoints() != self._bgplot.numPoints():
                contur.config.contur_log.warning(
                    "%s : stack and backround have unequal n points. Skipping." % self._bgplot.path())
                return False

            for i in range(0, len(self._stack.points())):
                self._stack.points()[i].setY(
                    self._stack.points()[i].y() * self._scaleFactorSig / self._scaleFactorData +
                    self._bgplot.points()[i].y())
                # set these to include only the MC stat errors, since that is what is used in the test
                self._stack.points()[i].setYErrs(
                    self.signal.points()[i].yErrs()[
                        0] * self._scaleFactorSig / self._scaleFactorData,
                    self.signal.points()[i].yErrs()[1] * self._scaleFactorSig / self._scaleFactorData)

    def __doScale(self):
        """Private function to perform the normalisation of the signal
        """

        if self.signal.type() != "Scatter2D":
            return

        for i in range(0, len(self.signal.points())):
            self.signal.points()[i].setY(self.signal.points()[
                i].y() * self._scaleFactorSig)
            self.signal.points()[i].setYErrs(
                map(lambda x: x * self._scaleFactorSig, self.signal.points()[i].yErrs()))

    def __fillBucket(self):
        """Create a conturBucket, contains the observables from this histogram and their correlation plus statistical metrics

        :Built members:
            * *conturBucket* (:class:`contur.conturBucket`) --
              Automatically filled bucket containing statistical test pertaining to this histogram

        """
        if len(self._ref.points()) != len(self.signal.points()):
            contur.config.contur_log.error(
                "Reference data and signal had unequal number of points so discarded from %s" % self.signal.path())
            return

        # TODO: rewrite this so you just give it the signal, background and measurement 
        # TODO: signal errors are symmetrised here. Does that make any difference? (should not)
        self._bucket = contur.factories.Bucket(s=self.signal.yVals(), bg=self._background.yVals(), nobs=self._ref.yVals(),
                                    serr=[((abs(x.yErrs()[0])) + (abs(x.yErrs()[1]))) * 0.5 for x in
                                          self.signal.points()],
                                    cov=self._cov,
                                    uncov=self._uncov, theorycov=self._thCov, theoryuncov=self._thUncov,
                                    nuisErrs=self._nuisErrs, thErrs=self._thErrs,
                                    ratio=self._isRatio,
                                    useTheory=(
                                        self._theoryComp or contur.config.useTheory),
                                    lumi=self._lumi,
                                    tags=rivet.stripOptions(self.signal.path()))
        self._bucket.pools = self.pool
        self._bucket.subpools = self.subpool

        # Lastly for convenience and user info get the bucket CLs and attach it to a member of histFactory
        self._CLs = self._bucket.CLs

    @property
    def CLs(self):
        """CLs score derived from conturBucket

        :return: CLs ``float``

        """
        return self._CLs

    @property
    def background(self):
        """Background model, scaled if required

        :return: background :class:`YODA.Scatter2D`

        """
        return self._background

    @property
    def ref(self):
        """
        Reference data, observed numbers input to test, scaled if required

        :return: ref :class:`YODA.Scatter2D`

        """
        return self._ref

    @property
    def stack(self):
        """Stacked, unscaled Signal+background for plotting

        :return: stack :class:`YODA.Scatter2D`

        """
        return self._stack

    @property
    def sigplot(self):
        """Signal for plotting

        :return: sigplot :class:`YODA.Scatter2D`

        """
        return self._sigplot

    @property
    def refplot(self):
        """Reference data for plotting

        :return: refplot :class:`YODA.Scatter2D`

        """
        return self._refplot

    @property
    def bgplot(self):
        """Background data for plotting

        :return: bgplot :class:`YODA.Scatter2D`

        """
        return self._bgplot

    @property
    def thyplot(self):
        """Theory for plotting

        :return: thyplot :class:`YODA.Scatter2D`

        """
        return self._thyplot

    @property
    def isScaled(self):
        """Bool representing if there is additional scaling applied on top of luminosity

        :return: isScaled ``bool``

        """
        return self._isScaled

    @property
    def gotTheory(self):
        """Bool representing if a theory prediction was found for the input signal

        :return: gotTheory ``bool``

        """
        return self._gotTh

    @property
    def scaleFactorSig(self):
        """Scale factor applied to the signal histogram/scatter, derived generally from input nEv and xs

        :return: scaleFactorSig ``float``


        """
        return self._scaleFactorSig

    @property
    def scaleFactorData(self):
        """Scale factor applied to the refdata histogram/scatter

        :return: scaleFactorData ``float``


        """
        return self._scaleFactorData

    @property
    def conturBucket(self):
        """Returns the conturBucket, including statistical metrics, representing this histogram

        :return: conturBucket :class:`contur.conturBucket` -- Automatically filled bucket containing statistical test pertaining to this histogram

        """
        return self._bucket


    def __repr__(self):
        if not self.signal.path():
            tag = "Unidentified Source"
        else:
            tag = self.signal.path()
        return "%s from %s, with %s" % (self.__class__.__name__, tag, self._bucket)


class YodaFactory(object):
    """Class controlling Conturs YODA file processing ability

    :param yodaFilePath:
        Valid :mod:`os.path` filesystem YODA file location
    :type yodaFilePath: ``string``

    :Built variables:
        * **conturBuckets** (:class:`contur.conturBucket`) --
          List of all conturBuckets created from YODA file
        * **sortedBuckets** (:class:`contur.conturBucket`) --
          Reduced conturBuckets list after removing all statistically correlated sets
        * **conturPoint** (:class:`contur.conturBucket`) --
          Single representative test statistic point combining all in sortedBuckets

    :Keyword Arguments:
        * *outdir* (``string``) --
          path to output plot objects
        * *gridRun* (``bool``) --
          Set true to restrict generating plot objects when in grid mode
        * *noStack* (``bool``) --
          Mark true to not stack the signal on background in plotting (*cosmetic*)

    """

    def __init__(self, yodaFilePath, outdir="plots", noStack=False):
        self.yodaFilePath = yodaFilePath
        # self.mcHistos,self.xSec,self.Nev=util.getHistos(yodaFilePath)

        self._conturBuckets = []
        self._sortedBuckets = []
        self._conturPoint = contur.factories.CombinedBucket()

        self._OutputDir = outdir
        self._NoStack = noStack

        self.__getConturBuckets()

    def __getConturBuckets(self):
        """Private function to collect all of the conturBuckets from a YODA file

        :Built variables:
        * **conturBuckets** (:class:`contur.conturBucket`) --
          List of all conturBuckets created from YODA file
        """
        # TODO: move this wrapper to a common contur util area
        try:
            from tqdm import tqdm as pbar
        except ImportError:
            def pbar(iterable, **kwargs):
                return iterable

        mc_histos, x_sec, nev = contur.util.getHistos(self.yodaFilePath)
        for k, v in mc_histos.items():
            aopaths = [x.path() for x in v.values()]
            if not contur.config.refLoaded:
                aopaths = contur.factories.init_ref(aopaths)

            for k2, v2 in pbar(v.items(), total=len(v)):

                if contur.data.validHisto(v2.path()):
                    contur.config.contur_log.debug(
                        "Valid path {}".format(v2.path()))
                    histo = contur.factories.HistFactory(
                        v2, x_sec, nev, aopaths=aopaths)

                    # if we are running on theory only, require it exists.
                    if histo._ref is not None and (histo.gotTheory or not contur.config.theoryOnly):

                        contur.config.contur_log.debug(
                            "Processing measurement {}".format(histo.signal.path()))

                        # write out the plot .dat files
                        if not contur.config.gridMode:
                            plotdirs = [os.path.abspath(
                                os.path.dirname(f)) for f in self.yodaFilePath]
                            plotparser = contur.util.mkStdPlotParser(plotdirs, )
                            contur.util.writeHistoDat(self.yodaFilePath, plotparser, self._OutputDir,
                                               self._NoStack, histo)
                        if histo._bucket:
                            # I don't like this but check that a CLs value was built or throw the bucket out
                            if histo._bucket.CLs is not None:
                                contur.config.contur_log.debug(
                                    "CLs for %s is %f" % (v2.path(), histo._bucket.CLs))
                                self._conturBuckets.append(histo._bucket)



        # we cannot pickle yoda objects so we just declare them in this scope when we are scrubbing the yodafile
        del mc_histos, x_sec, nev

    def sortBuckets(self, omitted_pools=""):
        """Private function to sort the built list of conturBuckets

        :Built variables:
            * **sortedBuckets** (:class:`contur.conturBucket`) --
              Reduced conturBuckets list after removing all statistically correlated sets
        :Keyword Arguments:
            * *omittedPools* (``string``) --
              String of analysis pools to omit
        """
        pools = []
        [pools.append(x) for x in [
            item.pools for item in self._conturBuckets] if x not in pools]
        for p in pools:
            if omitted_pools == p:
                continue
            anas = []
            [anas.append(x) for x in
             [contur.config.ANALYSIS.search(item.tags).group() for item in self._conturBuckets if
              item.tags and item.pools == p] if
             x not in anas]
            for a in anas:
                subpools = []
                for item in self._conturBuckets:
                    if item.pools == p and a in item.tags:
                        if item.subpools not in subpools and item.subpools is not None:
                            subpools.append(item.subpools)

                if len(subpools) > 0:
                    result = {}
                    for sp in subpools:
                        result[sp] = contur.factories.CombinedBucket()

                    for k, v in result.items():
                        # Remove the point if it ends up in a group
                        # Tags need to store which histo contribute to this point.
                        for y in self._conturBuckets:
                            if y.subpools == k and a in y.tags:
                                result[k].addTS(y)
                                if len(result[k].tags) > 0:
                                    result[k].tags += ","
                                result[k].tags += y.tags

                        v.calcCLs()
                        v.pools = p
                        v.tags = result[k].tags

                    # add the max subpool back into the list of points with the pool tag set but no subpool
                    [self._conturBuckets.append(v) for k, v in
                     result.items()]  # if v.CLs == max([z.CLs for z in result.values()])

        for p in pools:
            if not p == omitted_pools:
                [self._sortedBuckets.append(item) for item in self._conturBuckets if item.CLs == max(
                    [x.CLs for x in self._conturBuckets if x.pools == p]) and item.pools == p and item.pools not in [
                     x.pools
                     for x in
                     self._sortedBuckets]]

        # once all the points are sorted and the representative of each pool is put into _sortedPoints, work out the
        # final exclusion
        self.buildFinal(omitted_pools)

    def _sortSortedBuckets(self):
        """Private function to sort the built list of sortedBuckets *used for resorting after a merging exclusively*

        :Built variables:
            * **sortedBuckets** (:class:`contur.conturBucket`) --
              Reduced conturBuckets list after removing all statistically correlated sets
        :Keyword Arguments:
            * *omittedPools* (``string``) --
              String of analysis pools to omit
        """
        pools = []
        [pools.append(x) for x in [
            item.pools for item in self.sortedBuckets] if x not in pools]
        for p in pools:
            anas = []
            [anas.append(x) for x in
             [contur.config.ANALYSIS.search(item.tags).group() for item in self.sortedBuckets if
              item.tags and item.pools == p]
             if
             x not in anas]
            for a in anas:
                subpools = []
                [subpools.append(x) for x in
                 [item.subpools for item in self.sortedBuckets if item.pools == p and a in item.tags] if
                 x not in subpools]
                if subpools[0]:
                    result = {}
                    for sp in subpools:
                        result[sp] = contur.factories.CombinedBucket()
                    for k, v in result.items():
                        # Remove the point if it ends up in a group
                        # Tags need to store which histo contribute to this point.
                        for y in self.sortedBuckets:
                            if y.subpools == k and a in y.tags:
                                result[k].addTS(y)
                                # result[k].addPoint(y)
                                if len(result[k].tags) > 0:
                                    result[k].tags += ","
                                result[k].tags += y.tags
                        v.calcCLs()
                        v.pools = p
                        v.tags = result[k].tags
                    # add the max subpool back into the list of points with the pool tag set but no subpool
                    [self._sortedBuckets.append(v) for k, v in
                     result.items()]  # if v.CLs == max([z.CLs for z in result.values()])

        tempStore = []

        for p in pools:
            [tempStore.append(item) for item in self.sortedBuckets if item.CLs == max(
                [x.CLs for x in self.sortedBuckets if x.pools == p]) and item.pools == p and item.pools not in [x.pools
                                                                                                                for x in
                                                                                                                tempStore]]
        self._sortedBuckets = tempStore
        # once all the points are sorted and the representative of each pool is put into _sortedBuckets, work out the
        # final exclusion
        self.buildFinal()

        # cleanup some bulk we don't need
        if hasattr(self, '_conturBuckets'):
            del self._conturBuckets
        if hasattr(self, 'yodaFilePath'):
            del self.yodaFilePath

    def buildFinal(self, omittedPool=""):
        """Build a final conturPoint combining all buckets into one test

        :Built variables:
            * **conturPoint** (:class:`contur.conturBucket`) --
              single combined bucket of all tests
        :Keyword Arguments:
            * *omittedPools* (``string``) --
              String of analysis pools to omit
        """
        self._conturPoint = contur.factories.CombinedBucket()
        for x in self._sortedBuckets:
            if x.pools != omittedPool:
                self._conturPoint.addTS(x)
        self._conturPoint.calcCLs()

    @property
    def sortedBuckets(self):
        """Reduced conturBuckets list from processed YODA file after removing all statistically correlated sets

        :return: List of conturBuckets (:class:`contur.conturBucket`)

        """
        return self._sortedBuckets

    @property
    def conturBuckets(self):
        """Reduced conturBuckets list from processed YODA file after removing all statistically correlated sets

        :return: List of conturBuckets (:class:`contur.conturBucket`)

        """
        return self._conturBuckets

    @property
    def conturPoint(self):
        """single representative conturBucket represnting allowed combination from input YODA file

        :return: conturBucket (:class:`contur.conturBucketBucket`)

        """
        return self._conturPoint

    def __repr__(self):
        return "%s with %s buckets, holding %s" % (self.__class__.__name__, len(self.conturBuckets), self.conturPoint)
