POOLCOLORS = {
    # ==================================
    # ATLAS
    # ==================================
    # mixed
    "ATLAS_3L": {
        "color" : "crimson",
    },
    "ATLAS_4L": {
        "color" : "magenta",
    },
    "ATLAS_GAMMA": {
        "color" : "yellow",
    },
    "ATLAS_GAMMA_MET": {
        "color" : "darkgoldenrod",
    },
    "ATLAS_JETS": {
        "color" : "silver",
    },

    "ATLAS_LLJET": {
        "color" : "orange",
    },
    "ATLAS_EEJET": {
        "color" : "orangered",
    },
    "ATLAS_MMJET": {
        "color" : "darkorange",
    },

    "ATLAS_LMETJET": {
        "color" : "blue",
    },
    "ATLAS_EMETJET": {
        "color" : "cadetblue",
    },
    "ATLAS_MMETJET": {
        "color" : "navy",
    },

    "ATLAS_METJET": {
        "color" : "green",
    },
    "ATLAS_TTHAD": {
        "color" : "snow",
    },
    "ATLAS_WW": {
        "color" : "cornflowerblue",
    },
    "ATLAS_ZZ": {
        "color" : "firebrick",
    },

    "ATLAS_LLMET": {
        "pools" : ["ATLAS_7_LLMET"],        
        "color" : "greenyellow",
    },
    "ATLAS_LL_GAMMA": {
        "pools" : ["ATLAS_7_EE_GAMMA", "ATLAS_7_MM_GAMMA", "ATLAS_13_LL_GAMMA"],
        "color" : "mediumseagreen",
    },


    # 7 TeV
    "ATLAS_7_DY": {
        "pools" : ["ATLAS_7_HMDY", "ATLAS_7_LMDY"],
        "color" : "tomato",
    },
    "ATLAS_7_LMET_GAMMA": {
        "pools" : ["ATLAS_7_EMET_GAMMA", "ATLAS_7_MMET_GAMMA"],
        "color" : "lightgreen",
    },

    # 8 TeV
    "ATLAS_8_HMDY_LL": {
        "pools" : ["ATLAS_8_HMDY_EL", "ATLAS_8_HMDY_MU"],
        "color" : "crimson",
    },
    "ATLAS_8_MM_GAMMA": {
        "pools" : ["ATLAS_8_MM_GAMMA"],
        "color" : "indianred",
    },

    # 13 TeV
    "ATLAS_13_HMDY": {
        "pools" : ["ATLAS_13_HMDY"],
        "color" : "darkolivegreen",
    },

    # ==================================
    # CMS
    # ==================================
    # mixed
    "CMS_3L": {
        "color" : "saddlebrown",
    },
    "CMS_4L": {
        "color" : "hotpink",
    },
    "CMS_GAMMA": {
        "color" : "gold",
    },
    "CMS_GAMMA_MET": {
        "color" : "goldenrod",
    },
    "CMS_JETS": {
        "color" : "dimgrey",
    },

    "CMS_LLJET": {
        "color" : "salmon",
    },
    "CMS_EEJET": {
        "color" : "lightsalmon",
    },
    "CMS_MMJET": {
        "color" : "darksalmon",
    },

    "CMS_LMETJET": {
        "color" : "powderblue",
    },
    "CMS_EMETJET": {
        "color" : "deepskyblue",
    },
    "CMS_MMETJET": {
        "color" : "steelblue",
    },

    "CMS_METJET": {
        "color" : "darkgreen",
    },
    "CMS_TTHAD": {
        "color" : "wheat",
    },
    "CMS_WW": {
        "color" : "royalblue",
    },
    "CMS_ZZ": {
        "color" : "white",
    },

    # 13 TeV
    "CMS_13_HMDY": {
        "pools" : ["CMS_13_HMDY"],
        "color" : "seagreen",
    },

    # ==================================
    # other
    # ==================================
    "LHCB_7_LLJET": {
        "pools" : ["LHCB_7_EEJET", "LHCB_7_MMJET"],
        "color" : "brown",
    },

    "other": {
        "pools": ["other"],
        "color" : "white",
    },
}


# Create energy-specific internal pool groupings for colour tinting
for poolGroupName, poolGroup in POOLCOLORS.items():
    if "pools" in poolGroup:
        continue
    split = poolGroupName.split("_")
    poolGroup["pools"] = ["_".join([split[0], x]+split[1:])
                          for x in ["7", "8", "13"]]

      
