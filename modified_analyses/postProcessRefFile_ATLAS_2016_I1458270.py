#!/usr/bin/env python
import yoda
import os,re


aos=yoda.read("HEPData-ins1458270-v1-yoda.yoda")

output={}

allowedHistos=["d04-","d05-","d06-","d07-","d08-","d09-","d10-"]
srCuts={
             "d04-":1200.,
             "d05-":1600,
             "d06-":2000.,
             "d07-":2200.,
             "d08-":1600.,
             "d09-":1600.,
             "d10-":2000
             }

srNames={
             "d04-":"2jl",
             "d05-":"2jm",
             "d06-":"2jt",
             "d07-":"4jt",
             "d08-":"5j",
             "d09-":"6jm",
             "d10-":"6jt"
             }


for aoName in aos.keys():
  skip=True
  srCut=None
  srName=None
  for ah in allowedHistos:
    if ah in aoName: 
      srCut=srCuts[ah]
      srName=srNames[ah]
      skip=False
  if skip: continue
  if 'y03' in aoName: continue
  ao= aos[aoName]
  #aoName=aoName.replace("ATLAS_2016_I1458270","ATLAS_2016_I1458270_Meff")
  #ao.path=ao.path.replace("ATLAS_2016_I1458270","ATLAS_2016_I1458270_Meff")
  #ao.setName(ao.name.replace("ATLAS_2016_I1458270","ATLAS_2016_I1458270_Meff"))

  if 'y02' in aoName :
    ao.path=ao.path.replace('y02','y01').replace('REF','THY')
    #ao.setName(ao.name.replace('y02','y01').replace('REF','THY'))

  if 'y01' in aoName :
     for p in ao.points:
       p.setErr(2,p.y**0.5)
  
  counter=yoda.Scatter2D(ao.path.rpartition("/")[0]+"/"+srName, srName)
  counter.addPoint(0.5,0,(0.5,0.5),(0.,0.))
  
  for p in ao.points:
    if p.xMin >= srCut:
      y= counter.points[0].y
      yErrs= counter.points[0].yErrs
      counter.points[0].y= (y+p.y)
      counter.points[0].yErrs=((yErrs[0]**2+p.yErrs[0]**2)**0.5,(yErrs[1]**2+p.yErrs[1]**2)**0.5)

  counter.points[0].yErrs=(2*counter.points[0].yErrs[0],2*counter.points[0].yErrs[1])
  output[ao.path]=ao
  output[ao.path.rpartition("/")[0]+"/"+srName]=counter
  

  print ao.path

f=open('ATLAS_2016_I1458270.yoda','w')

yoda.writeYODA(output,f)

