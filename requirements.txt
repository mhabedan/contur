#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile --output-file=requirements.txt requirements.in
#
attrs==19.3.0             # via pytest
configobj==5.0.6          # via -r requirements.in
cycler==0.10.0            # via matplotlib
future==0.18.2            # via -r requirements.in
kiwisolver==1.2.0         # via matplotlib
matplotlib==3.3.0         # via -r requirements.in
more-itertools==8.4.0     # via pytest
numpy==1.19.1             # via -r requirements.in, matplotlib, pandas, scipy
packaging==20.4           # via pytest
pandas==1.0.5             # via -r requirements.in
pillow==7.2.0             # via matplotlib
pluggy==0.13.1            # via pytest
py==1.9.0                 # via pytest
pyparsing==2.4.7          # via matplotlib, packaging
pyslha==3.2.4             # via -r requirements.in
pytest==5.4.3             # via -r requirements.in
python-dateutil==2.8.1    # via matplotlib, pandas
pytz==2020.1              # via pandas
pyyaml==5.3.1             # via -r requirements.in
scipy==1.5.2              # via -r requirements.in
six==1.15.0               # via configobj, cycler, packaging, python-dateutil
tex2pix==0.3.1            # via pyslha
tqdm==4.48.0              # via -r requirements.in
wcwidth==0.2.5            # via pytest
