#!/usr/bin/env bash
mydir="$( dirname "${BASH_SOURCE[0]}" )"

# some cd commands issue terminal instructions, so pipe them away
export CONTURMODULEDIR="$( cd "$mydir" 2>&1 > /dev/null && echo "$PWD" )"
export RIVET_DATA_PATH=$RIVET_DATA_PATH:$CONTURMODULEDIR/modified_analyses:$CONTURMODULEDIR/modified_analyses/Theory
export RIVET_ANALYSIS_PATH=$RIVET_ANALYSIS_PATH:$CONTURMODULEDIR/modified_analyses
export RIVET_PLOT_PATH=$RIVET_PLOT_PATH:$CONTURMODULEDIR/modified_analyses
export RIVET_INFO_PATH=$RIVET_INFO_PATH:$CONTURMODULEDIR/modified_analyses

export PYTHONPATH=$PYTHONPATH:$CONTURMODULEDIR/AnalysisTools/contur
export PATH=$CONTURMODULEDIR/AnalysisTools/contur/bin:${PATH}

# This file won't exist until make has been run.
ALIST=$CONTURMODULEDIR/AnalysisTools/GridSetup/GridPack/analysis-list 
if [ -f $ALIST ]; then
    source $ALIST
fi

chmod +x $CONTURMODULEDIR/contur-visualiser/contur-visualiser
#now run some checks and reassure the user with messages if all looks good
python $CONTURMODULEDIR/AnalysisTools/contur/bin/check-contur-deps